sudo apt-get update
sudo apt-get upgrade
sudo raspi-config nonint do_ssh 0
sudo raspi-config nonint do_i2c 0
sudo raspi-config nonint do_spi 0
sudo raspi-config nonint do_rgpio 0

sudo apt-get install -y make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl openssl libssl-dev libffi-dev

wget https://www.python.org/ftp/python/3.8.6/Python-3.8.6.tgz
tar -xvzf Python-3.8.6.tgz
cd Python-3.8.6

./configure
make
make test
sudo make install

wget -qO- https://repos.influxdata.com/influxdb.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdb.gpg > /dev/null
export DISTRIB_ID=$(lsb_release -si); export DISTRIB_CODENAME=$(lsb_release -sc)
echo "deb [signed-by=/etc/apt/trusted.gpg.d/influxdb.gpg] https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list > /dev/null
sudo apt-get update && sudo apt-get install influxdb2
influxd
xdg-open "http://localhost:8086/"

lund
coldjig
go to data -> API Tokens -> "username's Token" Copy to clipboard
vim endCap_influx.ini

pip3 install pytest remi numpy influxdb coloredlogs pyserial influxdb-client spidev rpi.gpio pi-plates smbus2 pypubsub six

echo 'export PYTHONPATH=$PYTHONPATH:/home/pekman/work/coldjiglib2' >> ~/.bashrc
echo 'export PYTHONPATH=$PYTHONPATH:/home/pekman/work/coldjiglib2/modules' >> ~/.bashrc
echo 'export PYTHONPATH=$PYTHONPATH:/home/pekman/work/coldjiglib2/ITSDAQ_COMM' >> ~/.bashrc
echo 'export PYTHONPATH=$PYTHONPATH:/home/pekman/work/coldbox_controller_webgui/GUImodules' >> ~/.bashrc

mkdir log
python3 coldbox_controller_webgui.py -v -c configs/config_endCap.conf

## Burn Raspberry Pi OS image
 * Download and run the raspberry pi imager: https://www.raspberrypi.com/software/
 * Choose raspberry PI OS 32-bit
 * Choose your SD card
 * Select "Write"
## Setup Raspberry Pi without monitor (WIFI) 
* Via a terminal, open the newly created boot volume of the SD card 
* Create empty ssh file without extension:
	* `touch ssh`
* Create file to hold wifi access credentials:
    * `vim wpa_supplicant.conf`
        ```
        country=US
        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        update_config=1
        network={
            ssid="NETWORK-NAME"
            psk="NETWORK-PASSWORD"
        }
        ```
    * Remember to use your specific `country`, `ssid`, and `psk`
* Eject the SD card, put it in the RPI and power it on
* Find the IP address of the RPI via yout network administration
* SSH into your RPI with graphical support
    * `ssh -Y pi@GIVEN_IP_ADDRESS`
* Logged in to your RPI, change the password
    * `passwd`

## Installing and setting up the Coldjig library
* Uphrade and update the RPI
    * `sudo apt-get update`
    * `sudo apt-get upgrade`
* Enable RPI interfaces
    * `sudo raspi-config`
        * Select 8. Update
        * Select 3. Interface Options
            * Enable SSH (I2 SSH) (required for remote access)
            * Enable SPI (I4 SPI) (required for PiPlate DAQC2Plate)
            * Enable Remote GPI (I8 Remote GPIO)
            * Enable I2C (I5 I2C)
                * You might see this error "FATAL: Module i2c-dev not found in directory /lib/modules/5.10.17-v7+". This will be fixed upon rebooting
    * Select "Finish" to Exit the tool
* Execute as one command
    ```      
    sudo apt-get install -y make build-essential libssl-dev zlib1g-dev \
    libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
    libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl
    ```
* `sudo apt-get install openssl libssl-dev`
### Python environment using Pyenv and Pipenv
* `curl https://pyenv.run | bash`
* `export PYENV_ROOT="$HOME/.pyenv"`
* `command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"`
* `eval "$(pyenv init -)"`
* Execute as one command
    ```
    echo -e 'if shopt -q login_shell; then' \
        '\n  export PYENV_ROOT="$HOME/.pyenv"' \
        '\n  export PATH="$PYENV_ROOT/bin:$PATH"' \
        '\n eval "$(pyenv init --path)"' \
        '\nfi' >> ~/.bashrc
    echo -e 'if [ -z "$BASH_VERSION" ]; then'\
        '\n  export PYENV_ROOT="$HOME/.pyenv"'\
        '\n  export PATH="$PYENV_ROOT/bin:$PATH"'\
        '\n  eval "$(pyenv init --path)"'\
        '\nfi' >>~/.profile
    echo 'eval "$(pyenv init -)"' >> ~/.bashrc
    ```
* Execute the appended commands
    * `source .bashrc`
* Install Pyenv
    * `pyenv install 3.8.6`
* Create and enter a working directory
    * `mkdir work; cd work`
* Set the working directory's python version
    * `pyenv local 3.8.6`
* Double check that it worked
    * `pyenv versions`
* Install pipenv
    * `python -m pip install --user pipenv`
    * `sudo apt-get install pipenv`
* Clone the Coldjig repositories
    * `git clone https://gitlab.cern.ch/ColdJigDCS/coldbox_controller_webgui.git`
    * `git clone https://gitlab.cern.ch/ColdJigDCS/coldjiglib2.git`
* Enter the web GUI directory
    * `cd coldbox_controller_webgui`
* Set environment parameters    
    * `source setenv.sh`
* Install packages
    * `python -m pipenv install --skip-lock`
    * `pipenv install urllib3`
    * `pipenv install --keep-outdated pyserial`
    * `pipenv install --keep-outdated Pypubsub`

## Subsequent setups and startups
* Start the environment shell
    * `python -m pipenv shell`
* Run the coldjig web GUI
    * `python coldbox_controller_webgui.py -v -c configs/config_endCap.conf`
