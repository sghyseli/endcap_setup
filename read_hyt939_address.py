from smbus2 import SMBus, i2c_msg
import time
import sys
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)
GPIO.output(17, GPIO.HIGH)

address = int(sys.argv[1],16) #0x28

print(f"Trying to read from new address: {address}")
bus = SMBus(1)

# Do a read
msg = i2c_msg.write(address,[0])
bus.i2c_rdwr(msg)
time.sleep(0.2)

# Do the read
msg = i2c_msg.read(address,4)
bus.i2c_rdwr(msg)

# Print the message
data = list(msg)

humidity = ((data[0] & 0x3f) << 8 | data[1]) * (100.0 / 0x3fff)
temperature = (data[2] << 8 | (data[3] & 0xfc)) * (165.0 / 0xfffc) - 40.0

print(f'RH:{humidity}  Temp:{temperature}')


bus.close()

GPIO.output(17, GPIO.LOW)
GPIO.cleanup()
