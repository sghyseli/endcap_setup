import RPi.GPIO as GPIO
from smbus2 import SMBus, i2c_msg
import time
import sys
import os

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)
GPIO.output(17, GPIO.HIGH)
time.sleep(0.25)
os.system("sudo i2cdetect -y 1")
GPIO.output(17, GPIO.LOW)

old_address = input("Old Address: ")
new_address = input("New Address: ")
old_hexAddress = int(old_address,16) #0x28
new_hexAddress = int(new_address,16) #0x27
time.sleep(0.25)

bus = SMBus(1)
GPIO.output(17, GPIO.HIGH)
bus.i2c_rdwr(i2c_msg.write(old_hexAddress,[0xA0,0x00,0x00]))
time.sleep(0.02)
bus.i2c_rdwr(i2c_msg.write(old_hexAddress,[0x5C,0x00,new_hexAddress]))
time.sleep(0.02)
bus.i2c_rdwr(i2c_msg.write(old_hexAddress,[0x80,0x00,0x00]))
print(f"Changed from {old_address} to {new_address}")
time.sleep(0.25)
GPIO.output(17, GPIO.LOW)
bus.close()

print(f"Trying to read from new address: {new_address}")
bus = SMBus(1)

GPIO.output(17, GPIO.HIGH)
time.sleep(0.25)

# Do a read
msg = i2c_msg.write(new_hexAddress,[0])
bus.i2c_rdwr(msg)
time.sleep(0.2)

# Do the read
msg = i2c_msg.read(new_hexAddress,4)
bus.i2c_rdwr(msg)

# Print the message
data = list(msg)

humidity = ((data[0] & 0x3f) << 8 | data[1]) * (100.0 / 0x3fff)
temperature = (data[2] << 8 | (data[3] & 0xfc)) * (165.0 / 0xfffc) - 40.0

print(f'RH:{humidity}  Temp:{temperature}')

time.sleep(0.25)
GPIO.output(17, GPIO.LOW)

bus.close()

GPIO.cleanup()
